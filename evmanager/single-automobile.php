<?php
/**
 * Created by PhpStorm.
 * User: magnus
 * Date: 2/21/18
 * Time: 1:05 PM
 */
get_header();
?>
    <div id="primary">
        <div id="content" role="main">

            <?php while ( have_posts() ) : the_post(); ?>                

                <h1><?php the_field('mark'); ?></h1>

                <h1><?php the_field('model'); ?></h1>

               <?php // check if the repeater field has rows of data
                if( have_rows('repiter') ):

                // loop through the rows of data
                while ( have_rows('repiter') ) : the_row();

                // display a sub field value
                    the_sub_field('engine');?>
                    <br>
                    <?php
                    // display a sub field value
                    the_sub_field('hpower');

                endwhile;

                else :

                // no rows found

                endif;


                $images = get_field('images');
                $size = 'medium'; // (thumbnail, medium, large, full or custom size)

                if( $images ): ?>
                <ul>
                    <?php foreach( $images as $image ): ?>
                        <li>
                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>

                <h1><?php the_field('weight'); ?></h1>

                <p><?php the_content(); ?></p>

            <?php endwhile; // end of the loop. ?>

        </div><!-- #content -->
    </div><!-- #primary -->

<?php get_footer();