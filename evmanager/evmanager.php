<?php
/**
 * Plugin Name: Evmanager
 * Description: Manege of post type event
 * Version: 1.0.0
 * Author: Balabanov Alexander
 */

add_action( 'init', 'evmanager_setup_post_types' );
function evmanager_setup_post_types(){
	// Регистрируем тип записи "events"
	register_post_type('events',
		array(
			'labels' => array(
				'name' => __( 'Events' ),
				'singular_name' => __( 'Event' ),
			),
			'public' => true,
			'show_ui' => true,
			'show_in_menu'	=> true,
			'has_archive' => true,
			'taxonomies'  => array('meeting'),

		)
	);
}


add_action( 'init', 'create_events_taxonomies' );

function create_events_taxonomies()
{
	$labels = array(
		'name' => _x('Meeting', 'taxonomy meeting name'),
		'singular_name' => _x('Meeting', 'taxonomy singular name'),
		'search_items' => __('Search Meetings'),
		'all_items' => __('All Meetings'),
		'parent_item' => __('Parent Meeting'),
		'parent_item_colon' => __('Parent Meeting:'),
		'edit_item' => __('Edit Meeting'),
		'update_item' => __('Update Meeting'),
		'add_new_item' => __('Add New Meeting'),
		'new_item_name' => __('New Meeting Name'),
		'menu_name' => __('Meeting'),
	);

	// Добавляем древовидную таксономию 'meeting' 
	register_taxonomy('meeting', array('events'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'meeting'),
	));
}

register_activation_hook( __FILE__, 'evmanager_install' );
function evmanager_install(){
	// Запускаем функцию регистрации типа записи
	evmanager_setup_post_types();

	// Сбрасываем настройки ЧПУ, чтобы они пересоздались с новыми данными
	flush_rewrite_rules();
}


register_deactivation_hook( __FILE__, 'evmanager_deactivation' );
function evmanager_deactivation() {
	// Тип записи не регистрируется, а значит он автоматически удаляется - его не нужно удалять как-то еще.

	// Сбрасываем настройки ЧПУ, чтобы они пересоздались с новыми данными
	flush_rewrite_rules();
}

add_action( 'current_screen', 'current_screen_hook' );
function current_screen_hook( $current_screen ){
	if ( 'events' == $current_screen->post_type && ($current_screen->action == 'add' || $current_screen->base == 'post')) {

		add_action('add_meta_boxes', 'my_extra_fields', 1);

	}
}

function my_extra_fields() {
	add_meta_box( 'extra_fields', 'Additional fields', 'extra_fields_box_func', 'events', 'normal', 'high'  );
}


function extra_fields_box_func( $post ){
	?>
	<p><label><input type="date" name="extra[date]" value="<?php echo get_post_meta($post->ID, 'date', 1); ?>"/>Event date</label></p>


	<p><select name="extra[select]">
			<?php $sel_v = get_post_meta($post->ID, 'select', 1); ?>

			<option value="1" <?php selected( $sel_v, '1' )?> >Open</option>
			<option value="2" <?php selected( $sel_v, '2' )?> >Closed</option>

		</select> Event Type</p>

	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
	<?php
}

add_action('save_post', 'my_extra_fields_update', 0);

/* Сохраняем данные, при сохранении поста */

function my_extra_fields_update( $post_id ){
	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
	if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

	if( !isset($_POST['extra']) ) return false;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	$_POST['extra'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['extra'] as $key=>$value ){
		if( empty($value) ){
			delete_post_meta($post_id, $key); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
	}
	return $post_id;
}

// Register and load the widget
function wpev_load_widget() {
	register_widget( 'wpev_widget' );
}
add_action( 'widgets_init', 'wpev_load_widget' );

// Creating the widget
class wpev_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

// Base ID of your widget
			'wpev_widget',

// Widget name will appear in UI
			__('WPEvent Widget', 'wpev_widget_domain'),

// Widget description
			array( 'description' => __( 'Sample widget ', 'wpev_widget_domain' ), )
		);
	}

// Creating widget front-end

	public function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'] );
		$amount = apply_filters( 'widget_amount', $instance['amount'] );
		$type_event = apply_filters( 'widget_type_event', $instance['type_event'] );

		if($type_event=="Open"){
				$type_event=1;
				}else{
				$type_event =2;
				}

// This is where you run the code and display the output
		$args = array(
			'numberposts' => $amount,
			'category'    => 0,
			'orderby'     => 'date',
			'order'       => 'DESC',
			'include'     => array(),
			'exclude'     => array(),
			'meta_query' => array(
				'relation' => 'AND',
						array(
							'key' => 'select',
							'value' => $type_event
						),
						array(
							'key' => 'date',
							'value' => date("Y-m-d"),
							'compare' => '>='
						)
			),
			'post_type'   => 'events',
			'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
		);





		global $post;
		$myposts = get_posts($args);
		if ( ! empty( $title ) )
			echo  $title;

		echo "<p><label>Count of event: ".count($myposts)."</label></p>";
		
		foreach( $myposts as $post ){
			setup_postdata( $post );
			?>
			<?php if ( have_posts() ) : ?>

				<div class="post">

					<!-- Выводим заголовок поста, как ссылку на сам пост. -->
					<h2><a href="<?php the_permalink() ?>" title="Ссылка на: <?php the_title_attribute(); ?>"><?php echo the_title(); ?></a></h2>
					<p><label><?php echo get_post_meta($post->ID, 'date', 1); ?> Date of Event</label></p>

				</div> <!-- закрываем основной тег div -->


			<?php else: ?>

				<p>Нет постов в цикле.</p>

			<?php endif;

		}wp_reset_postdata();


	}

// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
				$title = $instance[ 'title' ];
				}
			else {
				$title = __( 'New title', 'wpb_widget_domain' );
			}

		if ( isset( $instance[ 'amount' ] ) ) {
				$amount = $instance[ 'amount' ];
			}
			else {
				$amount = __( '1', 'wpb_widget_domain' );
			}

		if ( isset( $instance[ 'type_event' ] ) ) {
				$type_event = $instance[ 'type_event' ];
				}
			else {
				$type_event = __( 'Open', 'wpb_widget_domain' );
			}
// Widget admin form
		?>


		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<label for="<?php echo $this->get_field_id( 'amount' ); ?>"><?php _e( 'Amount:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'amount' ); ?>" name="<?php echo $this->get_field_name( 'amount' ); ?>" type="text" value="<?php echo esc_attr( $amount ); ?>" />
		<label for="<?php echo $this->get_field_id( 'type_event' ); ?>"><?php _e( 'Type_event:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'type_event' ); ?>" name="<?php echo $this->get_field_name( 'type_event' ); ?>" type="text" value="<?php echo esc_attr( $type_event ); ?>" />

		<?php
	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['amount'] = ( ! empty( $new_instance['amount'] ) ) ? strip_tags( $new_instance['amount'] ) : '';
		$instance['type_event'] = ( ! empty( $new_instance['type_event'] ) ) ? strip_tags( $new_instance['type_event'] ) : '';
		return $instance;
	}


	function get_events_meta( $post_id, $key = '', $single = false ) {
		return get_metadata('events', $post_id, $key, $single);
	}
} // Class wpb_widget ends here

//Short Code
add_shortcode('evmanager_short','evman_shortcode_func');

function evman_shortcode_func($atts){
	extract(shortcode_atts(array(
		"amounts" => 1,
		"type_events" => 'open'
	), $atts));

	//$widget = new wpev_widget();
//	$content = $widget->widget($args, $instance );

	$title = "Evmanager";
	$amount = $atts['amounts'];
	$type_event = $atts['type_events'];

	if($type_event=="Open"||$type_event=="open"){
		$type_event=1;
	}else{
		$type_event =2;
	}

// This is where you run the code and display the output
	$args = array(
		'numberposts' => $amount,
		'category'    => 0,
		'orderby'     => 'date',
		'order'       => 'DESC',
		'include'     => array(),
		'exclude'     => array(),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'select',
				'value' => $type_event
			),
			array(
				'key' => 'date',
				'value' => date("Y-m-d"),
				'compare' => '>='
			)
		),
		'post_type'   => 'events',
		'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
	);





	global $post;
	$myposts = get_posts($args);
	//if ( ! empty( $title ) )
	$content = "". $title;

	$content .= "<p><label>Count of event: ".count($myposts)."</label></p>";

	foreach( $myposts as $post ){
		setup_postdata( $post );

/*		title="Ссылка на: <?php the_title_attribute(); ?>"><?php echo the_title(); ?><!--</a></h2>-->
<!--<p><label>--><?php //echo get_post_meta($post->ID, 'date', 1); ?><!-- Date of Event</label></p>-->
		*/

		$content.="<div> ";
		$content.="<a href=".get_the_permalink($post).">$post->post_title</a>";
		$content.="<br>";
		$content.="<p><label>".get_post_meta($post->ID, 'date', 1)."Date of Event</label></p>";

		//$content.=" \" ".the_title()."</a></h2>";
		$content.="</div>";


	}wp_reset_postdata();

return $content;

}